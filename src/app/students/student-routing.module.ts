import { Routes, RouterModule, Router } from "@angular/router";
import { StudentsViewComponent } from "./view/students.view.component";
import { StudentsAddComponent } from "./add/students.add.component";
import { StudentsComponent } from "./list/students.component";
import { NgModule } from "@angular/core";
import { FileNotFoundComponent } from "../shared/file-not-found/file-not-found.component";
import { NameComponent } from "../name/name.component";

const StudentRoutes: Routes = [
   { path: 'view', component: StudentsViewComponent },
    { path: 'add', component: StudentsAddComponent },
    { path: 'list', component: NameComponent },
    { path: 'detail/:id', component: StudentsViewComponent}
]

@NgModule({
    imports:[
        RouterModule.forRoot(StudentRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class StudentRoutingModule{

}